<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( count( $this->session->menu_module ) > 0 ) { ?>


    <div class="container" id="homepage">


<?php $this->load->view('alpha_list'); ?>

    </div>

<?php } else { ?>

<div class="container">
<div class="row">
    		<div class="col-md-4 col-md-offset-4">
    			<div class="panel panel-danger">
    				<div class="panel-heading">
    					<h3 class="panel-title bold">Account Restricted</h3>
    				</div>
    				<div class="panel-body text-center">
    				Your account have not been granted any access to the system! <br/> Please contact system administrator!
					</div>
    			</div>
    		</div>
</div>
</div>
<?php } ?>

<?php $this->load->view('footer'); ?>