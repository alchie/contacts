<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('User_accounts_model');
		$this->load->model('Names_list_model');
	}

	public function index() {

		$stats = new $this->User_accounts_model('ua');
		$stats->set_select('(SELECT count(*) FROM user_accounts) as users_count');
		$this->template_data->set('stats', $stats->get());
		
		$this->load->view('welcome/welcome', $this->template_data->get_data());
	}

	public function ajax($action='') {
		$results = array();
		switch($action) {
			case 'search_name':

				if( ! $this->_isAuth('contacts', 'contacts', 'view', 'welcome', true) ) {
					break;
				}

				$names = new $this->Names_list_model;

				if( $this->input->get('term') ) {
					$names->set_where('full_name LIKE "%' . $this->input->get('term') . '%"');
				}

				$names->set_order('full_name', 'ASC');
				$names->set_limit(0); 

				foreach($names->populate() as $name) {
					$results[] = array(
						'label' => $name->full_name,
						'id' => $name->id,
						'redirect'=> site_url( 'contacts/contacts/' . $name->id ),
						);
				}
			break;
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $results ));
	}

}
